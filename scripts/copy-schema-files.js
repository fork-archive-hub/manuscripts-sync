const { join } = require('path')
const { writeFileSync, createReadStream, createWriteStream } = require('fs')

const distDir = join(__dirname, '..', 'dist')
const nodeModulesDir = join(__dirname, '..', 'node_modules')
const jsonSchemaDir = join(
  nodeModulesDir,
  '@manuscripts',
  'manuscripts-json-schema'
)

// copy typescript types from @manuscripts/manuscripts-json-schema
createReadStream(join(jsonSchemaDir, 'dist', 'types', 'types.ts')).pipe(
  createWriteStream(join(distDir, 'types.ts'))
)

// create typescript definition for syncFn
const syncDefinition = `\
export declare function syncFn(doc: any, oldDoc: any): void;
`

writeFileSync(join(distDir, 'manuscriptsSyncFn.d.ts'), syncDefinition, 'utf8')
writeFileSync(join(distDir, 'manuscriptsReSyncFn.d.ts'), syncDefinition, 'utf8')
writeFileSync(join(distDir, 'derivedDataSyncFn.d.ts'), syncDefinition, 'utf8')
writeFileSync(join(distDir, 'discussionsSyncFn.d.ts'), syncDefinition, 'utf8')

// create typescript index definition
const indexDefinition = `\
export { syncFn as manuscriptsSyncFn } from './manuscriptsSyncFn'
export { syncFn as manuscriptsReSyncFn } from './manuscriptsReSyncFn'
export { syncFn as derivedDataSyncFn } from './derivedDataSyncFn'
export { syncFn as discussionsSyncFn } from './discussionsSyncFn'
export * from './types';
`

writeFileSync(join(distDir, 'index.d.ts'), indexDefinition, 'utf8')

const indexSrc = `\
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.manuscriptsSyncFn = require("./manuscriptsSyncFn").syncFn;
exports.manuscriptsReSyncFn = require("./manuscriptsReSyncFn").syncFn;
exports.derivedDataSyncFn = require("./derivedDataSyncFn").syncFn;
exports.discussionsSyncFn = require("./discussionsSyncFn").syncFn;
`

writeFileSync(join(distDir, 'index.js'), indexSrc, 'utf8')
