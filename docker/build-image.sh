#!/bin/bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VERSION="$($DIR/../bin/get-version.sh)"
IMAGE_NAME=${IMAGE_NAME-"registry.gitlab.com/mpapp-public/manuscripts-sync/sync_gateway"}
SHORT_IMAGE_NAME=$(basename $IMAGE_NAME) # let's assume the image name looks like a relative path
SYNC_GATEWAY_VERSIONED_TAG="${IMAGE_NAME}:${VERSION}"
echo $SYNC_GATEWAY_VERSIONED_TAG

npm install
npm run build

mkdir -p local

if [ "$COPY_LOCAL_PACKAGES" = "true" ]; then
  echo "Copying @manuscripts/manuscripts-json-schema, @manuscripts/manuscripts-sync into 'local'."
  rm -rf local
  mkdir -p local/@manuscripts
  cp -R dist local/@manuscripts/manuscripts-sync
  # NOTE! Trailing slash in the source directory is meaningful (avoid repeating last path component in target directory)
  rsync --exclude='.git/' -aL node_modules/@manuscripts/manuscripts-json-schema/ local/@manuscripts/manuscripts-json-schema
fi

if [ "$PUBLISH" = "true" ]; then
  if [[ -z "${CI_JOB_TOKEN}" ]]; then
    echo $GITLAB_ACCESS_TOKEN | docker login -u $GITLAB_USER --password-stdin registry.gitlab.com
  else
    echo $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
  fi
fi

echo "Building & tagging…"
cd $DIR/..
docker build \
--build-arg MANUSCRIPTS_SYNC_VERSION="@$VERSION" \
--build-arg COPY_LOCAL_PACKAGES="${COPY_LOCAL_PACKAGES}" \
-t ${SHORT_IMAGE_NAME} \
-f ./docker/sync_gateway/Dockerfile .

docker tag "$SHORT_IMAGE_NAME" "$IMAGE_NAME"
docker tag "$SHORT_IMAGE_NAME" "${SYNC_GATEWAY_VERSIONED_TAG}"

if [ "$PUBLISH" = "true" ]; then
  docker push $IMAGE_NAME
fi
