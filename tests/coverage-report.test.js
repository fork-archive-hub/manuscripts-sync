const test = require('tape')
const { coverage } = require('./utils')

const {
  SCRIPT_NAME: MANUSCRIPTS_SCRIPT_NAME,
} = require('./manuscripts.test.js')
const {
  SCRIPT_NAME: DERIVED_DATA_SCRIPT_NAME,
} = require('./derived-data.test.js')

test.onFinish(() => {
  coverage(global.__coverage__, [
    MANUSCRIPTS_SCRIPT_NAME,
    DERIVED_DATA_SCRIPT_NAME,
  ])
})
