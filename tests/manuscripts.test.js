const test = require('tape')
const vm = require('vm')
const sinon = require('sinon')
const { createInstrumenter } = require('istanbul-lib-instrument')

const { manuscriptsSyncFn } = require('../dist')

const SCRIPT_NAME = 'manuscriptsSyncFn.js'

module.exports = { SCRIPT_NAME }

// instrument syncFn (add a counter above every line)
const instrumentedCode = createInstrumenter({}).instrumentSync(
  manuscriptsSyncFn.toString(),
  SCRIPT_NAME
)

// share coverage between vm executions
global.__coverage__ = {}

function execute(obj, oldObj = null, sandboxConfigure) {
  const channel = sinon.spy()
  const access = sinon.spy()
  const requireAccess = sinon.spy()
  const requireUser = sinon.spy()
  const requireAdmin = sinon.spy()
  const sandbox = {
    access,
    channel,
    obj,
    oldObj,
    requireAccess,
    requireAdmin,
    requireUser,
    Object,
    Array,
    __coverage__,
  }
  if (sandboxConfigure) {
    sandboxConfigure(sandbox)
  }
  // Create JS context where our scripts will be executed.
  const context = vm.createContext(sandbox)
  // Execute main script with validate function
  const mainScript = new vm.Script(instrumentedCode)
  mainScript.runInContext(context)
  // Run validate against obj
  const testScript = new vm.Script('syncFn(obj, oldObj)')
  try {
    testScript.runInContext(context)
    return sandbox
  } catch (e) {
    console.error(e)
    throw e
  }
}

test('random objects without MP-prefix', t => {
  t.plan(2)

  t.throws(() => {
    execute({ objectType: 'Foo', _id: 'Foo:foo' }, null)
  })

  // t.throws(() => {
  //   execute({ objectType: 'Foo' }, null)
  // })

  t.throws(() => {
    execute({ _id: 'Foo' }, null)
  })
})

test('object undeletion', t => {
  t.plan(4)

  const validParagraphElement = {
    _id: 'MPParagraphElement:AB6961A0-ECF7-4977-B74D-41F97103BD12',
    containerID: 'MPProject:AB6961A0-ECF7-4977-B74D-41F97103BD12',
    contents: '<p >asdasdasd</p>',
    createdAt: 1564056646,
    elementType: 'p',
    manuscriptID: 'MPManuscript:D4BCD5C4-3E81-484A-A4ED-B1EE8DF19212',
    objectType: 'MPParagraphElement',
    paragraphStyle: 'MPParagraphStyle:CA130447-12C8-4E89-ADC3-1850BD602DEF',
    placeholderInnerHTML:
      'Start from here. Enjoy writing! - the Manuscripts Team.',
    sessionID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    updatedAt: 1564056646,
  }

  const validSection = {
    _id: 'MPSection:CBD3F118-1708-46D4-9F15-2D6832EC517D',
    containerID: 'MPProject:AB6961A0-ECF7-4977-B74D-41F97103BD12',
    createdAt: 1564056329,
    elementIDs: ['MPParagraphElement:CBD3F118-1708-46D4-9F15-2D6832EC517D'],
    manuscriptID: 'MPManuscript:D4BCD5C4-3E81-484A-A4ED-B1EE8DF19212',
    objectType: 'MPSection',
    path: ['MPSection:CBCFAFD6-D868-4288-8651-0B40F4668731'],
    priority: 1,
    sessionID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    title: '',
    updatedAt: 1564056876,
  }

  const validProject = {
    owners: ['User_ben@example.com'],
    writers: [],
    viewers: [],
    objectType: 'MPProject',
    _id: 'MPProject:foo',
  }

  t.ok(
    execute(
      Object.assign({}, validParagraphElement),
      Object.assign({}, validParagraphElement, { _deleted: true })
    )
  )

  t.ok(
    execute(
      Object.assign({}, validSection),
      Object.assign({}, validSection, { _deleted: true })
    )
  )

  t.throws(() => {
    execute(
      Object.assign({}, validProject),
      Object.assign({}, validProject, { _deleted: true })
    )
  })

  t.ok(
    execute(
      Object.assign({}, validSection, { _deleted: true }),
      Object.assign({}, validSection)
    )
  )
})

test('deleted doc mutations', t => {
  t.plan(1)

  const validCollaboration = {
    _deleted: true,
    invitingUserId: 'User_bill@example.com',
    invitedUserId: 'User_ben@example.com',
    objectType: 'MPCollaboration',
    _id: 'MPCollaboration:foobarbaz',
    createdAt: 231230131,
  }

  t.throws(() =>
    execute(
      Object.assign({}, validCollaboration, {
        invitedUserId: 'User_mark@example.com',
      }),
      Object.assign({}, validCollaboration)
    )
  )
})

test('objectType mutations', t => {
  t.plan(4)

  const validCollaboration = {
    invitingUserId: 'User_bill@example.com',
    invitedUserId: 'User_ben@example.com',
    objectType: 'MPCollaboration',
    _id: 'MPCollaboration:foobarbaz',
    createdAt: 231230131,
  }

  t.throws(() => {
    execute(
      Object.assign({}, validCollaboration, { objectType: 'MPProject' }),
      Object.assign({}, validCollaboration)
    )
  })

  const validProject = {
    owners: ['User_ben@example.com'],
    writers: [],
    viewers: [],
    objectType: 'MPProject',
    _id: 'MPProject:foo',
  }

  t.throws(() => {
    execute(
      Object.assign({}, validProject, { objectType: 'MPUserProfile' }),
      Object.assign({}, validProject)
    )
  }, 'deleted document cannot be mutated')

  t.throws(() => {
    execute(
      {
        objectType: 'MPFoo',
        _id: 'MPBar:baz',
      },
      {
        objectType: 'MPBar',
        _id: 'MPBar:baz',
      }
    )
  })

  t.throws(() => {
    execute(
      {
        objectType: 'MPAffiliation',
        _id: 'MPBorder:foo',
        containerID: 'MPProject:foo',
      },
      {
        objectType: 'MPBorder',
        _id: 'MPBorder:foo',
        containerID: 'MPProject:foo',
      }
    )
  })
})

test('containerID mutations are not allowed', t => {
  t.plan(1)

  const validObject = {
    objectType: 'MPBorder',
    _id: 'MPBorder:foo',
    containerID: 'MPProject:bar',
  }

  t.throws(() => {
    execute(
      Object.assign({}, validObject),
      Object.assign({}, validObject, { containerID: 'MPProject:foo' })
    )
  })
})

test('MPProject creation', t => {
  t.plan(4)

  const validObject = {
    createdAt: 231230131,
    updatedAt: 231230131,
    owners: ['User_ben@example.com'],
    writers: [],
    viewers: [],
    objectType: 'MPProject',
    _id: 'MPProject:foo',
  }

  t.ok(
    execute(Object.assign({}, validObject), null, sandbox => {
      sandbox.requireAdmin = sinon.spy(() => {
        throw 'admin required'
      })
    }).requireAdmin.called,
    'requireAdmin called when no oldDoc'
  )

  t.ok(
    execute(Object.assign({}, validObject), null, sandbox => {
      sandbox.requireAdmin = sinon.spy(() => {
        throw 'admin required'
      })
    }).requireUser.calledWith(validObject.owners[0]),
    'requireUser called when appropriate'
  )

  t.throws(() => {
    execute(Object.assign({}, validObject), null, sandbox => {
      sandbox.requireUser = sinon.spy(() => {
        throw 'user required'
      })
      sandbox.requireAdmin = sinon.spy(() => {
        throw 'admin required'
      })
    })
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { viewers: ['User_foo@bar.com'] }),
      null,
      sandbox => {
        sandbox.requireAdmin = sinon.spy(() => {
          throw 'admin required'
        })
      }
    )
  })
})

test('MPProject', t => {
  t.plan(16)

  const validObject = {
    createdAt: 231230131,
    updatedAt: 231230131,
    owners: ['User_ben@example.com'],
    writers: [],
    viewers: [],
    objectType: 'MPProject',
    _id: 'MPProject:foo',
  }

  t.doesNotThrow(() => {
    execute(Object.assign({}, validObject), null)
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { containerID: 'MPProject:foo' }),
      null
    )
  })

  t.throws(() => {
    execute(Object.assign({}, validObject, { owners: [] }), null)
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        writers: ['User_david@example.com', 'User_david@example.com'],
      }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { writers: ['User_ben@example.com'] }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { viewers: ['User_ben@example.com'] }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        owners: ['User_bill@example.com'],
        writers: ['User_bill@example.com'],
        viewers: ['User_ben@example.com'],
      }),
      null
    )
  })

  t.throws(() => {
    execute(
      {
        writers: [],
        viewers: [],
        objectType: 'MPProject',
        _id: 'MPProject:foo',
      },
      null
    )
  })

  const execution = execute(Object.assign({}, validObject), null)
  t.ok(
    execution.channel.calledWith([
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
    ]),
    'channel function called with project read and readwrite channels'
  )

  t.ok(
    execution.channel.neverCalledWith('MPProject'),
    'channel function called with MPProject object type channel'
  )

  t.ok(
    execution.channel.calledWith(`${validObject.owners[0]}-projects`),
    'channel function called with projects list channel for its owner'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith([]),
    'access function called with nothing'
  )

  const objectWithOwners = Object.assign({}, validObject, {
    owners: ['User_bill@example.com', 'User_ben@example.com'],
  })

  t.ok(
    execute(objectWithOwners, null).access.calledWith(objectWithOwners.owners, [
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
      `${validObject._id}-owner`,
      `${validObject._id}-bibitems`,
      `${validObject._id}-metadata`,
    ]),
    'access function called with correct channels when only owners'
  )

  const objectWithOwnersAndWriters = Object.assign({}, validObject, {
    owners: ['User_bill@flowers.com', 'User_ben@flowers.com'],
    writers: ['User_bowie@space.com'],
  })

  t.ok(
    execute(objectWithOwnersAndWriters, null).access.calledWith(
      objectWithOwnersAndWriters.writers,
      [
        `${validObject._id}-read`,
        `${validObject._id}-readwrite`,
        `${validObject._id}-bibitems`,
        `${validObject._id}-metadata`,
      ]
    ),
    'access function called with correct channels when owners and writers'
  )

  const objectWithViewers = Object.assign({}, validObject, {
    viewers: ['User_mark@fb.com', 'User_zuckerberg@fb.com'],
  })

  t.ok(
    execute(objectWithViewers, null).access.calledWith(
      objectWithViewers.viewers,
      [
        `${validObject._id}-read`,
        `${validObject._id}-bibitems`,
        `${validObject._id}-metadata`,
      ]
    ),
    'access function called with correct channels when readers'
  )

  const deletedDocObjectWithOwners = {
    _id: validObject._id,
    _deleted: true,
  }

  const deletedOldDocObjectWithOwners = Object.assign({}, validObject, {
    owners: ['User_mark@fb.com', 'User_zuckerberg@fb.com'],
  })

  delete deletedOldDocObjectWithOwners._deleted

  t.ok(
    execute(
      deletedDocObjectWithOwners,
      deletedOldDocObjectWithOwners
    ).access.calledWith(deletedOldDocObjectWithOwners.owners, [
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
      `${validObject._id}-owner`,
      `${validObject._id}-bibitems`,
      `${validObject._id}-metadata`,
    ]),
    'access function called with correct channels when deleted'
  )
})

test('MPLibraryCollection', t => {
  t.plan(18)

  const validObject = {
    createdAt: 231230131,
    updatedAt: 231230131,
    owners: ['User_ben@example.com'],
    writers: [],
    viewers: [],
    objectType: 'MPLibraryCollection',
    _id: 'MPLibraryCollection:foo',
    containerID: 'MPLibrary:foobar',
    name: 'Foobar',
  }

  t.doesNotThrow(() => {
    execute(Object.assign({}, validObject), null)
  })

  t.throws(() => {
    execute(Object.assign({}, validObject, { owners: [] }), null)
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        writers: ['User_david@example.com', 'User_david@example.com'],
      }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { writers: ['User_ben@example.com'] }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { viewers: ['User_ben@example.com'] }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        owners: ['User_bill@example.com'],
        writers: ['User_bill@example.com'],
        viewers: ['User_ben@example.com'],
      }),
      null
    )
  })

  t.throws(() => {
    execute(
      {
        writers: [],
        viewers: [],
        objectType: 'MPLibraryCollection',
        _id: 'MPLibraryCollection:foo',
      },
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        category: 'MPLibraryCollectionCategory:foobar',
      }),
      Object.assign({}, validObject)
    ),
      'category cannot be mutated'
  })

  const execution = execute(Object.assign({}, validObject), null)
  t.ok(
    execution.channel.calledWith([
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
    ]),
    'channel function called with project read and readwrite channels'
  )

  t.ok(
    execution.channel.neverCalledWith('MPLibraryCollection'),
    'channel function called with MPLibraryCollection object type channel'
  )

  t.ok(
    execution.channel.calledWith(
      `${validObject.owners[0]}-library-collections`
    ),
    'channel function called with library collections list channel for its owner'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith([]),
    'access function called with nothing'
  )

  const objectWithOwners = Object.assign({}, validObject, {
    owners: ['User_bill@example.com', 'User_ben@example.com'],
  })

  t.ok(
    execute(objectWithOwners, null).access.calledWith(objectWithOwners.owners, [
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
      `${validObject._id}-owner`,
      `${validObject._id}-bibitems`,
      `${validObject._id}-metadata`,
    ]),
    'access function called with correct channels when only owners'
  )

  const objectWithOwnersAndWriters = Object.assign({}, validObject, {
    owners: ['User_bill@flowers.com', 'User_ben@flowers.com'],
    writers: ['User_bowie@space.com'],
  })

  t.ok(
    execute(objectWithOwnersAndWriters, null).access.calledWith(
      objectWithOwnersAndWriters.writers,
      [
        `${validObject._id}-read`,
        `${validObject._id}-readwrite`,
        `${validObject._id}-bibitems`,
        `${validObject._id}-metadata`,
      ]
    ),
    'access function called with correct channels when owners and writers'
  )

  const objectWithViewers = Object.assign({}, validObject, {
    viewers: ['User_mark@fb.com', 'User_zuckerberg@fb.com'],
  })

  t.ok(
    execute(objectWithViewers, null).access.calledWith(
      objectWithViewers.viewers,
      [
        `${validObject._id}-read`,
        `${validObject._id}-bibitems`,
        `${validObject._id}-metadata`,
      ]
    ),
    'access function called with correct channels when readers'
  )

  t.ok(
    execute(validObject, null).channel.calledWith([
      `${validObject.containerID}-read`,
      `${validObject.containerID}-readwrite`,
    ]),
    'channel function called with containerID read and readwrite'
  )

  const deletedDocObjectWithOwners = {
    _id: validObject._id,
    _deleted: true,
  }

  const deletedOldDocObjectWithOwners = Object.assign({}, validObject, {
    owners: ['User_mark@fb.com', 'User_zuckerberg@fb.com'],
  })

  t.ok(
    execute(
      deletedDocObjectWithOwners,
      deletedOldDocObjectWithOwners
    ).access.calledWith(deletedOldDocObjectWithOwners.owners, [
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
      `${validObject._id}-owner`,
      `${validObject._id}-bibitems`,
      `${validObject._id}-metadata`,
    ]),
    'access function called with correct channels when deleted'
  )

  t.ok(() => {
    execute(
      Object.assign({}, validObject, { writers: ['User_zuckerberg@fb.com'] }),
      null
    ).requireUser.calledWith([
      'User_ben@example.com',
      'User_zuckerberg@fb.com',
    ]),
      'requireUser function called with owners and writers for library collection'
  })
})

test('deleted documents and channel access after deletion', t => {
  t.plan(2)

  const oldDoc = {
    _id: 'MPManuscript:CBE8C5BA-11CF-4CD8-B9E9-D6FF8EA92811',
    containerID: 'MPProject:8A4AD1F0-4471-4452-AD93-2C545A0FEB6F',
    createdAt: 1530009427,
    objectType: 'MPManuscript',
    sessionID: '8c2a9fc2-3172-4d5b-b77f-b7775389b9bb',
    title: 'foo',
    updatedAt: 1530009977,
  }

  const newDoc = {
    _id: 'MPManuscript:CBE8C5BA-11CF-4CD8-B9E9-D6FF8EA92811',
    containerID: 'MPProject:8A4AD1F0-4471-4452-AD93-2C545A0FEB6F',
    createdAt: 1530009427,
    objectType: 'MPManuscript',
    sessionID: '8c2a9fc2-3172-4d5b-b77f-b7775389b9bb',
    title: 'foo',
    updatedAt: 1530009977,
    _deleted: true,
  }

  t.ok(
    execute(newDoc, oldDoc).channel.calledWith([
      `${oldDoc.containerID}-read`,
      `${oldDoc.containerID}-readwrite`,
    ]),
    'channel function called with correct argument when deleted'
  )

  t.ok(
    execute(newDoc, oldDoc).requireAccess.calledWith(
      `${oldDoc.containerID}-readwrite`
    ),
    'requireAccess function called with correct argument when deleted'
  )
})

test('MPLibrary', t => {
  t.plan(16)

  const validObject = {
    createdAt: 231230131,
    updatedAt: 231230131,
    owners: ['User_ben@example.com'],
    writers: [],
    viewers: [],
    objectType: 'MPLibrary',
    _id: 'MPLibrary:foo',
  }

  t.doesNotThrow(() => {
    execute(Object.assign({}, validObject), null)
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { containerID: 'MPLibrary:foo' }),
      null
    )
  })

  t.throws(() => {
    execute(Object.assign({}, validObject, { owners: [] }), null)
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        writers: ['User_david@example.com', 'User_david@example.com'],
      }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { writers: ['User_ben@example.com'] }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, { viewers: ['User_ben@example.com'] }),
      null
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        owners: ['User_bill@example.com'],
        writers: ['User_bill@example.com'],
        viewers: ['User_ben@example.com'],
      }),
      null
    )
  })

  t.throws(() => {
    execute(
      {
        writers: [],
        viewers: [],
        objectType: 'MPLibrary',
        _id: 'MPLibrary:foo',
      },
      null
    )
  })

  const execution = execute(Object.assign({}, validObject), null)
  t.ok(
    execution.channel.calledWith([
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
    ]),
    'channel function called with project read and readwrite channels'
  )

  t.ok(
    execution.channel.neverCalledWith('MPLibrary'),
    'channel function called with MPLibrary object type channel'
  )

  t.ok(
    execution.channel.calledWith(`${validObject.owners[0]}-libraries`),
    'channel function called with library list channel for its owner'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith([]),
    'access function called with nothing'
  )

  const objectWithOwners = Object.assign({}, validObject, {
    owners: ['User_bill@example.com', 'User_ben@example.com'],
  })

  t.ok(
    execute(objectWithOwners, null).access.calledWith(objectWithOwners.owners, [
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
      `${validObject._id}-owner`,
      `${validObject._id}-bibitems`,
      `${validObject._id}-metadata`,
    ]),
    'access function called with correct channels when only owners'
  )

  const objectWithOwnersAndWriters = Object.assign({}, validObject, {
    owners: ['User_bill@flowers.com', 'User_ben@flowers.com'],
    writers: ['User_bowie@space.com'],
  })

  t.ok(
    execute(objectWithOwnersAndWriters, null).access.calledWith(
      objectWithOwnersAndWriters.writers,
      [
        `${validObject._id}-read`,
        `${validObject._id}-readwrite`,
        `${validObject._id}-bibitems`,
        `${validObject._id}-metadata`,
      ]
    ),
    'access function called with correct channels when owners and writers'
  )

  const objectWithViewers = Object.assign({}, validObject, {
    viewers: ['User_mark@fb.com', 'User_zuckerberg@fb.com'],
  })

  t.ok(
    execute(objectWithViewers, null).access.calledWith(
      objectWithViewers.viewers,
      [
        `${validObject._id}-read`,
        `${validObject._id}-bibitems`,
        `${validObject._id}-metadata`,
      ]
    ),
    'access function called with correct channels when readers'
  )

  const deletedDocObjectWithOwners = {
    _id: validObject._id,
    _deleted: true,
  }

  const deletedOldDocObjectWithOwners = Object.assign({}, validObject, {
    owners: ['User_mark@fb.com', 'User_zuckerberg@fb.com'],
  })

  t.ok(
    execute(
      deletedDocObjectWithOwners,
      deletedOldDocObjectWithOwners
    ).access.calledWith(deletedOldDocObjectWithOwners.owners, [
      `${validObject._id}-read`,
      `${validObject._id}-readwrite`,
      `${validObject._id}-owner`,
      `${validObject._id}-bibitems`,
      `${validObject._id}-metadata`,
    ]),
    'access function called with correct channels when deleted'
  )
})

test('mutating project channel access', t => {
  t.plan(3)

  const oldDoc = {
    createdAt: 231230131,
    updatedAt: 231230131,
    owners: ['User_ben@example.com'],
    writers: [],
    viewers: [],
    objectType: 'MPProject',
    _id: 'MPProject:foo',
  }

  const newWriterDoc = Object.assign({}, oldDoc, {
    writers: ['User_unavoidable@spam.com'],
  })

  t.ok(
    execute(newWriterDoc, oldDoc).requireAdmin.called,
    'requireAdmin called when writers array is mutated'
  )

  const extraOwnerDoc = Object.assign({}, oldDoc, {
    owners: oldDoc.owners.concat('User_unavoidable@spam.com'),
  })

  t.ok(
    execute(extraOwnerDoc, oldDoc).requireAdmin.called,
    'requireAdmin called when owners array is mutated'
  )

  const differentOwnerDoc = Object.assign({}, oldDoc, {
    owners: ['User_unavoidable@spam.com'],
  })

  t.ok(
    execute(differentOwnerDoc, oldDoc).requireAdmin.called,
    'requireAdmin called when owners array is replaced'
  )
})

test('project and channel access', t => {
  t.plan(3)

  const oldDoc = {
    createdAt: 231230131,
    updatedAt: 231230131,
    owners: ['User_ben@example.com'],
    writers: [],
    viewers: [],
    objectType: 'MPProject',
    _id: 'MPProject:foo',
  }

  const newDoc = Object.assign({}, oldDoc, {})

  t.ok(
    execute(newDoc, oldDoc).requireAccess.calledWith(`${oldDoc._id}-readwrite`),
    'requireAccess function called with correct argument when updated'
  )

  t.ok(
    execute(newDoc, null).requireAccess.notCalled,
    'requireAccess function not called when its a new doc'
  )

  const deletedDoc = {
    _id: oldDoc._id,
    _deleted: true,
  }

  t.ok(
    execute(deletedDoc, oldDoc).requireAccess.calledWith(
      `${oldDoc._id}-readwrite`
    ),
    'requireAccess function called with correct argument when deleted'
  )
})

test('deleted documents and channel access', t => {
  t.plan(2)

  const oldDoc = {
    _id: 'MPManuscript:C09A4BE4-4A4D-4129-AF23-F630E1CB5080',
    containerID: 'MPProject:8A4AD1F0-4471-4452-AD93-2C545A0FEB6F',
    createdAt: 1530009427,
    objectType: 'MPManuscript',
    sessionID: '8c2a9fc2-3172-4d5b-b77f-b7775389b9bb',
    title: 'foo',
    updatedAt: 1530009977,
  }

  const newDoc = { ...oldDoc, _deleted: true }

  t.ok(
    execute(newDoc, oldDoc).channel.calledWith([
      `${oldDoc.containerID}-read`,
      `${oldDoc.containerID}-readwrite`,
    ]),
    "deleted manuscript remains on container's read and readwrite channels post deletion"
  )

  t.ok(
    execute(newDoc, oldDoc).requireAccess.calledWith(
      `${oldDoc.containerID}-readwrite`
    ),
    "container's readwrite channel is required for deleting a manuscript"
  )
})

test('MPProject user access', t => {
  t.plan(13)

  const { access } = execute(
    {
      createdAt: 231230131,
      updatedAt: 231230131,
      objectType: 'MPProject',
      _id: 'MPProject:foo',
      owners: ['User_owner1', 'User_owner2', 'User_owner3'],
      writers: ['User_writer1', 'User_writer2'],
      viewers: ['User_viewer1'],
    },
    null
  )

  // 6 + 3
  // See code for the 3 additional calls
  //
  // This is important to test so it's _less likely_ we do something stupid.
  t.equals(access.callCount, 17)

  t.ok(
    access.calledWith('User_owner1', [
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_owner2', [
      'User_owner1-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_owner3', [
      'User_owner1-read',
      'User_owner2-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_writer1', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_writer2', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_viewer1', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
    ])
  )

  t.ok(access.calledWith('User_owner1', 'User_owner1-projects'))
  t.ok(access.calledWith('User_owner2', 'User_owner2-projects'))
  t.ok(access.calledWith('User_owner3', 'User_owner3-projects'))
  t.ok(access.calledWith('User_writer1', 'User_writer1-projects'))
  t.ok(access.calledWith('User_writer2', 'User_writer2-projects'))
  t.ok(access.calledWith('User_viewer1', 'User_viewer1-projects'))
})

test('MPLibraryCollection user access', t => {
  t.plan(13)

  const { access } = execute(
    {
      createdAt: 231230131,
      updatedAt: 231230131,
      objectType: 'MPLibraryCollection',
      _id: 'MPLibraryCollection:foo',
      containerID: 'MPLibrary:foobar',
      name: 'Foobar',
      owners: ['User_owner1', 'User_owner2', 'User_owner3'],
      writers: ['User_writer1', 'User_writer2'],
      viewers: ['User_viewer1'],
    },
    null
  )

  // 6 + 3
  // See code for the 3 additional calls
  //
  // This is important to test so it's _less likely_ we do something stupid.
  t.equals(access.callCount, 17)

  t.ok(
    access.calledWith('User_owner1', [
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_owner2', [
      'User_owner1-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_owner3', [
      'User_owner1-read',
      'User_owner2-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_writer1', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_writer2', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_viewer1', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
    ])
  )

  t.ok(access.calledWith('User_owner1', 'User_owner1-library-collections'))
  t.ok(access.calledWith('User_owner2', 'User_owner2-library-collections'))
  t.ok(access.calledWith('User_owner3', 'User_owner3-library-collections'))
  t.ok(access.calledWith('User_writer1', 'User_writer1-library-collections'))
  t.ok(access.calledWith('User_writer2', 'User_writer2-library-collections'))
  t.ok(access.calledWith('User_viewer1', 'User_viewer1-library-collections'))
})

test('MPLibrary user access', t => {
  t.plan(13)

  const { access } = execute(
    {
      createdAt: 231230131,
      updatedAt: 231230131,
      objectType: 'MPLibrary',
      _id: 'MPLibrary:foo',
      owners: ['User_owner1', 'User_owner2', 'User_owner3'],
      writers: ['User_writer1', 'User_writer2'],
      viewers: ['User_viewer1'],
    },
    null
  )

  // 6 + 3
  // See code for the 3 additional calls
  //
  // This is important to test so it's _less likely_ we do something stupid.
  t.equals(access.callCount, 17)

  t.ok(
    access.calledWith('User_owner1', [
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_owner2', [
      'User_owner1-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_owner3', [
      'User_owner1-read',
      'User_owner2-read',
      'User_writer1-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_writer1', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer2-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_writer2', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_viewer1-read',
    ])
  )

  t.ok(
    access.calledWith('User_viewer1', [
      'User_owner1-read',
      'User_owner2-read',
      'User_owner3-read',
      'User_writer1-read',
      'User_writer2-read',
    ])
  )

  t.ok(access.calledWith('User_owner1', 'User_owner1-libraries'))
  t.ok(access.calledWith('User_owner2', 'User_owner2-libraries'))
  t.ok(access.calledWith('User_owner3', 'User_owner3-libraries'))
  t.ok(access.calledWith('User_writer1', 'User_writer1-libraries'))
  t.ok(access.calledWith('User_writer2', 'User_writer2-libraries'))
  t.ok(access.calledWith('User_viewer1', 'User_viewer1-libraries'))
})

test('locked documents', t => {
  t.plan(2)

  const validObject = {
    createdAt: 231230131,
    updatedAt: 231230131,
    objectType: 'MPLibrary',
    _id: 'MPLibrary:foo',
    owners: ['User_owner1', 'User_owner2', 'User_owner3'],
    writers: ['User_writer1', 'User_writer2'],
    viewers: ['User_viewer1'],
  }

  t.ok(
    execute(Object.assign({}, validObject, { locked: true }), null).requireAdmin
      .called,
    'requireAdmin called when no oldDoc'
  )

  t.ok(
    execute(
      Object.assign({}, validObject),
      Object.assign({}, validObject, { locked: true })
    ).requireAdmin.called,
    'requireAdmin called when no oldDoc'
  )
})

test('MPUserProfile', t => {
  t.plan(6)

  const validObject = {
    createdAt: 231230131,
    updatedAt: 231230131,
    _id: 'MPUserProfile:foo',
    objectType: 'MPUserProfile',
    userID: 'User_mark@example.com',
    bibliographicName: {
      _id: 'MPBibliographicName:foo-bar',
      objectType: 'MPBibliographicName',
      given: 'Mark',
      family: 'Zuckerberg',
    },
    email: 'mark@example.com',
  }

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      `${validObject.userID}-readwrite`
    ),
    'channel function called with correct channel name'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      `${validObject.userID}-read`
    ),
    'channel function called with correct channel name'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).requireAccess.calledWith(
      `${validObject.userID}-readwrite`
    ),
    'requireAccess function called with correct channel name'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      validObject.userID,
      `${validObject._id}-readwrite`
    ),
    'access function called with correct user and channel name'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).requireAdmin.called,
    'requireAdmin called when no oldDoc'
  )

  t.ok(
    execute(Object.assign({}, validObject), Object.assign({}, validObject))
      .requireAdmin.notCalled,
    'requireAdmin not called when there is an oldDoc'
  )
})

test('MPPreferences', t => {
  t.plan(2)

  const validObject = {
    _id: 'MPPreferences:foo@bar.com',
    objectType: 'MPPreferences',
    nightMode: true,
  }

  t.ok(
    execute(Object.assign({}, validObject), null).requireUser.calledWith(
      validObject._id.replace('MPPreferences:', '')
    ),
    'requireUser function called with correct user name'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject._id.replace('MPPreferences:', '')
    ),
    'channel function called with correct channel name'
  )
})

test('invitations', t => {
  t.plan(8)

  const validObject = {
    _id: 'MPInvitation:5480f0bfe3b0f69beb8fe360adab156e06c614ff',
    invitingUserID: 'User_valid-user@manuscriptsapp.com',
    invitedUserEmail: 'valid-google@manuscriptsapp.com',
    invitedUserID: 'User_valid-google@manuscriptsapp.com',
    invitingUserProfile: {
      _id: 'MPUserProfile:foo',
      objectType: 'MPUserProfile',
      userID: 'User_mark@example.com',
      bibliographicName: {
        _id: 'MPBibliographicName:foo-bar',
        objectType: 'MPBibliographicName',
        given: 'Mark',
        family: 'Zuckerberg',
      },
      email: 'mark@example.com',
      createdAt: 231230131,
      updatedAt: 231230131,
    },
    message: 'Message',
    createdAt: 1522231220.927,
    updatedAt: 1522231220.927,
    objectType: 'MPInvitation',
  }

  t.doesNotThrow(() => {
    const doc = Object.assign({}, validObject)
    execute(doc, null)
  }, 'does not throw when no oldDoc')

  t.doesNotThrow(() => {
    const doc = Object.assign({}, validObject)
    const oldDoc = Object.assign({}, validObject)
    execute(doc, oldDoc)
  })

  t.ok(
    execute(Object.assign({}, validObject), Object.assign({}, validObject))
      .requireAdmin.called,
    'requireAdmin called when no mutations'
  )

  t.ok(
    execute(
      Object.assign({}, validObject, { invitingUserID: 'User_foo@bar.com' }),
      Object.assign({}, validObject)
    ).requireAdmin.called,
    'requireAdmin function called for mutations'
  )

  t.ok(
    execute({ ...validObject, _deleted: true }, Object.assign({}, validObject))
      .requireAdmin.called,
    'requireAdmin called when attempting to undelete an invitation'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.invitingUserID
    ),
    `channel function called with correct channel names ${validObject.invitingUserID}`
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.invitedUserID
    ),
    `channel function called with correct channel names ${validObject.invitedUserID}`
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      'User_valid-google@manuscriptsapp.com',
      ['User_valid-user@manuscriptsapp.com-read']
    )
  )
})

test('project invitations', t => {
  t.plan(10)

  const validObject = {
    _id: 'MPContainerInvitation:5480f0bfe3b0f69beb8fe360adab156e06c614ff',
    invitingUserID: 'User_valid-user@manuscriptsapp.com',
    invitedUserEmail: 'valid-google@manuscriptsapp.com',
    invitedUserID: 'User_valid-google@manuscriptsapp.com',
    containerTitle: 'A* Algorithm',
    containerID: 'MPProject:this-is-sparta',
    invitedUserName: 'Kratos',
    invitingUserProfile: {
      _id: 'MPUserProfile:foo',
      objectType: 'MPUserProfile',
      userID: 'User_mark@example.com',
      bibliographicName: {
        _id: 'MPBibliographicName:foo-bar',
        objectType: 'MPBibliographicName',
        given: 'Mark',
        family: 'Zuckerberg',
      },
      email: 'mark@example.com',
      createdAt: 231230131,
      updatedAt: 231230131,
    },
    role: 'Owner',
    message: 'Message',
    createdAt: 1522231220.927,
    updatedAt: 1522231220.927,
    objectType: 'MPContainerInvitation',
  }

  t.doesNotThrow(() => {
    const doc = Object.assign({}, validObject)
    execute(doc, null)
  }, 'does not throw when no oldDoc')

  t.doesNotThrow(() => {
    const doc = Object.assign({}, validObject)
    const oldDoc = Object.assign({}, validObject)
    execute(doc, oldDoc)
  })

  t.ok(
    execute(Object.assign({}, validObject), Object.assign({}, validObject))
      .requireAdmin.called,
    'requireAdmin called when no mutations'
  )

  t.ok(
    execute(
      Object.assign({}, validObject, { invitingUserID: 'User_foo@bar.com' }),
      Object.assign({}, validObject)
    ).requireAdmin.called,
    'requireAdmin function called for mutations'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.invitingUserID
    ),
    `channel function called with correct channel names ${validObject.invitingUserID}`
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.invitedUserID
    ),
    `channel function called with correct channel names ${validObject.invitedUserID}`
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      'User_valid-google@manuscriptsapp.com',
      ['User_valid-user@manuscriptsapp.com-read']
    )
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith([
      'MPProject:this-is-sparta-read',
    ]),
    'channel function called with correct channel names'
  )

  const validObject1 = Object.assign({}, validObject, {
    projectID: 'MPProject:this-is-sparta',
    objectType: 'MPProjectInvitation',
    _id: 'MPProjectInvitation:5480f0bfe3b0f69beb8fe360adab156e06c614ff',
  })

  delete validObject1.containerID
  delete validObject1.containerTitle

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith([
      'MPProject:this-is-sparta-read',
    ]),
    'channel function called with correct channel names'
  )

  const validObject2 = Object.assign({}, validObject)
  delete validObject2.invitedUserName

  t.doesNotThrow(() => {
    const doc = Object.assign({}, validObject2)
    const oldDoc = Object.assign({}, validObject2)
    execute(doc, oldDoc)
  })
})

test('MPCollaboration', t => {
  t.plan(10)

  const validObject = {
    invitingUserID: 'User_bill@example.com',
    invitedUserID: 'User_ben@example.com',
    objectType: 'MPCollaboration',
    _id: 'MPCollaboration:foobarbaz',
    // _rev?: '',
    createdAt: 231230131,
    updatedAt: 231230131,
  }

  t.doesNotThrow(() => {
    const doc = Object.assign({}, validObject)
    execute(doc, null)
  }, 'does not throw when no oldDoc')

  t.doesNotThrow(() => {
    const doc = Object.assign({}, validObject)
    const oldDoc = Object.assign({}, validObject)
    execute(doc, oldDoc)
  })

  t.doesNotThrow(() => {
    const doc = { _id: validObject._id, _deleted: true }
    const oldDoc = Object.assign({}, validObject)
    execute(doc, oldDoc)
  })

  t.throws(() => {
    const doc = Object.assign({}, validObject)
    const oldDoc = Object.assign({}, validObject, {
      invitingUserId: 'User_mark@example.com',
    })
    execute(doc, oldDoc)
  })

  t.throws(() => {
    const doc = Object.assign({}, validObject, {
      invitingUserId: 'User_mark@example.com',
    })
    const oldDoc = Object.assign({}, validObject)
    execute(doc, oldDoc)
  })

  t.ok(
    execute(
      { ...validObject, _deleted: true },
      Object.assign({}, validObject)
    ).requireUser.calledWith(validObject.invitingUserID),
    'Only inviting user can delete a Collaboration record'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).requireUser.calledWith(
      validObject.invitingUserID
    ),
    'Creating a Collaboration record allowed for inviting user ID'
  )

  t.ok(
    execute(
      { _id: validObject._id, _deleted: true },
      Object.assign({}, validObject)
    ).requireUser.calledWith(validObject.invitingUserID),
    'Deleting of a Collaboration record allowed for inviting user ID'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.invitingUserID
    ),
    `channel function called with correct channel names ${validObject.invitingUserID}`
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.invitedUserID
    ),
    `channel function called with correct channel names ${validObject.invitedUserID}`
  )
})

test('contained documents', t => {
  t.plan(8)

  const validObject = {
    updatedAt: 1515494608.245375,
    objectType: 'MPBorderStyle',
    _rev: '1-cf3758c6a77c031dcd8f617087c7493d',
    _id: 'MPBorderStyle:15326C7B-836D-4D6C-81EB-7E6CA6153E9A',
    containerID: 'MPProject:foo-bar-baz',
    manuscriptID: 'MPManuscript:zorb',
    title: 'Dotted',
    pattern: [1, 1],
    createdAt: 1515417692.476143,
    name: 'dotted',
    sessionID: '4D17753C-AF51-4262-9FBD-88D8EC7E8495',
    priority: 1,
  }

  t.doesNotThrow(() => {
    execute(Object.assign({}, validObject), null)
  })

  t.throws(() => {
    execute(Object.assign({}, validObject, { name: 1 }), null)
  })

  t.throws(() => {
    execute(Object.assign({}, validObject, { containerID: null }), null)
  })

  t.throws(() => {
    const obj = Object.assign({}, validObject)
    delete obj.containerID
    execute(obj, null)
  })

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith([
      `${validObject.containerID}-read`,
      `${validObject.containerID}-readwrite`,
    ]),
    'channel function called with containerID'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).requireAccess.calledWith(
      `${validObject.containerID}-readwrite`
    ),
    'requireAccess function called with "containerID-readwrite"'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.notCalled,
    'access function NOT called'
  )

  t.doesNotThrow(() => {
    const deletedDoc = { _id: validObject._id, _deleted: true }
    const deletedOldDoc = Object.assign({}, validObject)
    execute(deletedDoc, deletedOldDoc)
  })
})

test('_id must have objectType as prefix', t => {
  t.plan(2)

  const validObject = {
    _id: 'MPContributor:foobar',
    objectType: 'MPContributor',
    containerID: 'MPProject:foobarbaz',
    manuscriptID: 'MPManuscript:foobar',
    bibliographicName: {
      _id: 'MPBibliographicName:foobar',
      objectType: 'MPBibliographicName',
    },
    sessionID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
  }

  t.throws(() => {
    execute(Object.assign({}, validObject, { _id: 'MPProject:foobar' }), null)
  })

  t.doesNotThrow(() => {
    execute(Object.assign({}, validObject), null)
  })
})

test('undeleting objects', t => {
  t.plan(2)

  const validObject = {
    _id: 'MPUserProfile:foo',
    objectType: 'MPUserProfile',
    userID: 'User_mark@example.com',
    bibliographicName: {
      _id: 'MPBibliographicName:foo-bar',
      objectType: 'MPBibliographicName',
      given: 'Mark',
      family: 'Zuckerberg',
    },
    email: 'mark@example.com',
    createdAt: 231230131,
    updatedAt: 231230131,
  }

  t.ok(
    execute(Object.assign({}, validObject), {
      _id: validObject._id,
      _deleted: true,
    }).requireAdmin.called,
    'requireAdmin called when trying to update a previously deleted object'
  )

  t.throws(() => {
    execute(Object.assign({}, validObject, { email: 4 }), {
      _id: validObject._id,
      _deleted: true,
    })
  }, 'new document should be schema-validated when previously deleted')
})

test('MPContainerRequest', t => {
  t.plan(2)

  const validObject = {
    _id: 'MPContainerRequest:DF026E1B-394A-4A68-C761-9DB39349A714',
    objectType: 'MPContainerRequest',
    containerID: 'MPProject:990DC4B9-4AAE-4AEF-8630-04929F53B8EC',
    userID: 'User_foobar@manuscriptsapp.com',
    role: 'Writer',
    userProfile: {
      _id: 'MPUserProfile:foo-bar-baz',
      objectType: 'MPUserProfile',
      createdAt: 1515417692.477127,
      updatedAt: 1515494608.363229,
      bibliographicName: {
        _id: 'MPBibliographicName:foo-bar-baz',
        objectType: 'MPBibliographicName',
        createdAt: 1515417692.477127,
        updatedAt: 1515494608.363229,
      },
      userID: 'User_foobar@manuscriptsapp.com',
    },
    createdAt: 1454394584,
    updatedAt: 1454537867.959872,
  }

  t.ok(
    execute(Object.assign({}, validObject), null).requireAdmin.called,
    'requireAdmin called'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.containerID + '-owner'
    ),
    'channel function called correctly'
  )
})

test('MPCitationAlert', t => {
  t.plan(7)

  const validObject = {
    objectType: 'MPCitationAlert',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
    _rev: '1-cf3758c6a77c031dcd8f617087c7493d',
    _id: 'MPCitationAlert:15326C7B-836D-4D6C-81EB-7E6CA6153E9B',
    sessionID: '4D17753C-AF51-4262-9FBD-88D8EC7E8498',
    userID: 'User_foobar@manuscriptsapp.com',
    sourceDOI: '10.1007/978-981-13-0341-8_10',
    targetDOI: '10.1176/appi.psychotherapy.71101',
  }

  t.ok(
    execute(Object.assign({}, validObject), null).requireAdmin.called,
    'requireAdmin called'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.userID + '-citation-alerts'
    ),
    'channel function called correctly'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      validObject.userID,
      validObject.userID + '-citation-alerts'
    ),
    'access function called with correct properties'
  )

  t.ok(
    execute(
      Object.assign({}, validObject, {
        userID: 'User_foobar2@manuscriptsapp.com',
      }),
      Object.assign({}, validObject)
    ).requireAdmin.called,
    'requireAdmin called'
  )

  t.ok(
    execute(
      Object.assign({}, validObject, {
        sourceDOI: '10.1007/978-981-13-0341-8_11',
      }),
      Object.assign({}, validObject)
    ).requireAdmin.called,
    'requireAdmin called'
  )

  t.ok(
    execute(
      Object.assign({}, validObject, {
        targetDOI: '10.1176/appi.psychotherapy.71102',
      }),
      Object.assign({}, validObject)
    ).requireAdmin.called,
    'requireAdmin called'
  )

  t.ok(
    execute(
      Object.assign({}, validObject, {
        isRead: true,
      }),
      Object.assign({}, validObject)
    ).requireUser.called,
    'requireUser called'
  )
})

test('MPMutedCitationAlert', t => {
  t.plan(5)

  const validObject = {
    objectType: 'MPMutedCitationAlert',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
    _rev: '1-cf3758c6a77c031dcd8f617087c7493d',
    _id: 'MPMutedCitationAlert:15326C7B-836D-4D6C-81EB-7E6CA6153E9B',
    sessionID: '4D17753C-AF51-4262-9FBD-88D8EC7E8498',
    userID: 'User_foobar@manuscriptsapp.com',
    targetDOI: '10.1176/appi.psychotherapy.71101',
  }

  t.ok(
    execute(Object.assign({}, validObject), null).requireUser.calledWith(
      validObject.userID
    ),
    'requireUser called'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.userID + '-citation-alerts'
    ),
    'channel function called correctly'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      validObject.userID,
      validObject.userID + '-citation-alerts'
    ),
    'access function called with correct properties'
  )

  t.ok(
    execute(
      Object.assign({}, validObject, {
        userID: 'User_foobar2@manuscriptsapp.com',
      }),
      validObject
    ).requireAdmin.called,
    'requireAdmin called'
  )

  t.ok(
    execute(
      Object.assign({}, validObject, {
        targetDOI: 'newDoi',
      }),
      validObject
    ).requireAdmin.called,
    'requireAdmin called'
  )
})

test('MPBibliographyItem', t => {
  t.plan(9)

  const containerID = 'MPLibrary:1'

  const keywordIDs = ['MPLibraryCollection:a', 'MPLibraryCollection:b']

  const validObject = {
    _id: 'MPBibliographyItem:1',
    objectType: 'MPBibliographyItem',
    createdAt: 1571375482,
    updatedAt: 1571375482,
    containerID,
    keywordIDs,
    type: 'article',
    title: 'Test Article',
  }

  const result = execute(Object.assign({}, validObject), null)

  for (const keywordID of keywordIDs) {
    t.ok(
      result.requireAccess.calledWith(`${containerID}-readwrite`),
      'requireAccess function called with containerID readwrite channel'
    )

    t.ok(
      result.channel.calledWith(`${containerID}-bibitems`),
      'channel function called with containerID bibitems channel'
    )

    t.ok(
      result.requireAccess.calledWith(`${keywordID}-readwrite`),
      'requireAccess function called with keywordID readwrite channel'
    )

    t.ok(
      result.channel.calledWith([
        `${keywordID}-read`,
        `${keywordID}-readwrite`,
      ]),
      'channel function called with keywordIDs read channel'
    )
  }

  t.ok(
    execute(Object.assign({}, validObject, { keywordIDs: undefined }), null),
    'allow missing keywordIDs field'
  )
})

test('MPCommentAnnotation', t => {
  t.plan(7)

  const validObject = {
    _id: 'MPCommentAnnotation:foobar',
    objectType: 'MPCommentAnnotation',
    containerID: 'MPProject:foobarbaz',
    contents: '',
    target: '',
    manuscriptID: 'MPManuscript:foobarbaz',
    contributions: [
      {
        _id: 'MPContribution:foobarbaz',
        objectType: 'MPContribution',
        profileID: 'MPUserProfile:foobarbaz',
        timestamp: 1515494608.363229,
      },
    ],
    sessionID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
  }

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        contributions: [
          ...validObject.contributions,
          {
            _id: 'MPContribution:foobarbaz2',
            objectType: 'MPContribution',
            profileID: 'MPUserProfile:foobarbaz2',
            timestamp: 1515494608.363229,
          },
        ],
      })
    )
  }, 'Only one contribution allowed')

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        contributions: [
          {
            _id: 'MPContribution:foobarbaz2',
            objectType: 'MPContribution',
            profileID: 'MPUserProfile:foobarbaz2',
            timestamp: 1515494608.363229,
          },
        ],
      }),
      Object.assign({}, validObject)
    )
  }, 'contributions cannot be mutated')

  // Only users with editor or readwrite access should be able to update 'resolved' field.
  t.ok(
    execute(
      Object.assign({}, validObject, { resolved: true }),
      Object.assign({}, validObject)
    ).requireAccess.calledWith([
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Only users with annotator, editor or readwrite access should be able to create/update comments.
  t.ok(
    execute(Object.assign({}, validObject)).requireAccess.calledWith([
      `${validObject.containerID}-annotator`,
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Only the user who is the contributor on the document should have access to update comments.
  t.ok(
    execute(Object.assign({}, validObject)).requireAccess.calledWith(
      `${validObject.contributions[0].profileID}-readwrite`
    )
  )

  // The object should be added to read and readwrite channels.
  t.ok(
    execute(Object.assign({}, validObject)).channel.calledWith([
      `${validObject.containerID}-read`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Allow annotators to reject own comments.
  t.ok(
    execute(
      Object.assign({}, validObject, { resolved: false }),
      Object.assign({}, validObject)
    ).requireAccess.calledWith([
      `${validObject.contributions[0].profileID}-readwrite`,
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )
})

test('MPManuscriptNote', t => {
  t.plan(7)

  const validObject = {
    _id: 'MPManuscriptNote:foobar',
    objectType: 'MPManuscriptNote',
    containerID: 'MPProject:foobarbaz',
    contents: '',
    target: '',
    manuscriptID: 'MPManuscript:foobarbaz',
    contributions: [
      {
        _id: 'MPContribution:foobarbaz',
        objectType: 'MPContribution',
        profileID: 'MPUserProfile:foobarbaz',
        timestamp: 1515494608.363229,
      },
    ],
    sessionID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
    source: 'EMAIL',
  }

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        contributions: [
          ...validObject.contributions,
          {
            _id: 'MPContribution:foobarbaz2',
            objectType: 'MPContribution',
            profileID: 'MPUserProfile:foobarbaz2',
            timestamp: 1515494608.363229,
          },
        ],
      })
    )
  }, 'Only one contribution allowed')

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        contributions: [
          {
            _id: 'MPContribution:foobarbaz2',
            objectType: 'MPContribution',
            profileID: 'MPUserProfile:foobarbaz2',
            timestamp: 1515494608.363229,
          },
        ],
      }),
      Object.assign({}, validObject)
    )
  }, 'contributions cannot be mutated')

  // Only users with editor or readwrite access should be able to update 'resolved' field.
  t.ok(
    execute(
      Object.assign({}, validObject, { resolved: true }),
      Object.assign({}, validObject)
    ).requireAccess.calledWith([
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Only users with annotator, editor or readwrite access should be able to create/update manuscript notes.
  t.ok(
    execute(Object.assign({}, validObject)).requireAccess.calledWith([
      `${validObject.containerID}-annotator`,
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Only the user who is the contributor on the document should have access to update manuscript notes.
  t.ok(
    execute(Object.assign({}, validObject)).requireAccess.calledWith(
      `${validObject.contributions[0].profileID}-readwrite`
    )
  )

  // The object should be added to read and readwrite channels.
  t.ok(
    execute(Object.assign({}, validObject)).channel.calledWith([
      `${validObject.containerID}-read`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Allow annotators to reject own comments.
  t.ok(
    execute(
      Object.assign({}, validObject, { resolved: false }),
      Object.assign({}, validObject)
    ).requireAccess.calledWith([
      `${validObject.contributions[0].profileID}-readwrite`,
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )
})

test('MPCorrection', t => {
  t.plan(7)

  const validObject = {
    _id: 'MPCorrection:foobar',
    objectType: 'MPCorrection',
    containerID: 'MPProject:foobarbaz',
    manuscriptID: 'MPManuscript:foobarbaz',
    contributions: [
      {
        _id: 'MPContribution:foobarbaz',
        objectType: 'MPContribution',
        profileID: 'MPUserProfile:foobarbaz',
        timestamp: 1515494608.363229,
      },
    ],
    sessionID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    snapshotID: 'MPSnapshot:40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    commitChangeID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
    status: { label: 'proposed', editorProfileID: 'MPUserProfile:foobarbaz' },
  }

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        contributions: [
          ...validObject.contributions,
          {
            _id: 'MPContribution:foobarbaz2',
            objectType: 'MPContribution',
            profileID: 'MPUserProfile:foobarbaz2',
            timestamp: 1515494608.363229,
          },
        ],
      })
    )
  }, 'Only one contribution allowed')

  t.throws(() => {
    execute(
      Object.assign({}, validObject, {
        contributions: [
          {
            _id: 'MPContribution:foobarbaz2',
            objectType: 'MPContribution',
            profileID: 'MPUserProfile:foobarbaz2',
            timestamp: 1515494608.363229,
          },
        ],
      }),
      Object.assign({}, validObject)
    )
  }, 'contributions cannot be mutated')

  // Only users with editor or readwrite access should be able to update status.
  t.ok(
    execute(
      Object.assign({}, validObject, {
        status: {
          label: 'accepted',
          editorProfileID: 'MPUserProfile:foobarbaz2',
        },
      }),
      Object.assign({}, validObject)
    ).requireAccess.calledWith([
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Only users with annotator, editor or readwrite access should be able to create/update corrections.
  t.ok(
    execute(Object.assign({}, validObject)).requireAccess.calledWith([
      `${validObject.containerID}-annotator`,
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Only the user who is the contributor on the document should have access to update correction.
  t.ok(
    execute(Object.assign({}, validObject)).requireAccess.calledWith(
      `${validObject.contributions[0].profileID}-readwrite`
    )
  )

  // The object should be added to read and readwrite channels.
  t.ok(
    execute(Object.assign({}, validObject)).channel.calledWith([
      `${validObject.containerID}-read`,
      `${validObject.containerID}-readwrite`,
    ])
  )

  // Only users with editor or readwrite access should be able to update status.
  t.ok(
    execute(
      Object.assign({}, validObject, {
        status: {
          label: 'rejected',
          editorProfileID: 'MPUserProfile:foobarbaz2',
        },
      }),
      Object.assign({}, validObject)
    ).requireAccess.calledWith([
      `${validObject.contributions[0].profileID}-readwrite`,
      `${validObject.containerID}-editor`,
      `${validObject.containerID}-readwrite`,
    ])
  )
})

test('MPCommit', t => {
  t.plan(2)

  const validObject = {
    _id: 'MPCommit:foobar',
    objectType: 'MPCommit',
    containerID: 'MPProject:foobarbaz',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
    blame: [
      {
        from: 1,
        to: 2,
        commit: 'Some message',
      },
    ],
    steps: [],
    changeID: 'random',
  }

  t.ok(
    execute(Object.assign({}, validObject)).requireAccess.calledWith([
      `${validObject.containerID}-readwrite`,
      `${validObject.containerID}-annotator`,
      `${validObject.containerID}-editor`,
    ]),
    'Only users with annotator, editor or readwrite access should be able to create corrections.'
  )

  t.ok(
    execute(
      Object.assign({}, validObject),
      Object.assign({}, validObject, { changeID: 'someid' })
    ).requireAccess.calledWith(`${validObject.containerID}-readwrite`),
    'Only users with readwrite access should be able to update corrections.'
  )
})

test('MPManuscript', t => {
  t.plan(1)

  const validObject = {
    _id: 'MPManuscript:foobar',
    objectType: 'MPManuscript',
    containerID: 'MPProject:foobarbaz',
    sessionID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
  }

  t.ok(
    execute(Object.assign({}, validObject)).channel.calledWith(
      `${validObject.containerID}-metadata`
    )
  )
})

test('MPContributor', t => {
  t.plan(1)

  const validObject = {
    _id: 'MPContributor:foobar',
    objectType: 'MPContributor',
    containerID: 'MPProject:foobarbaz',
    manuscriptID: 'MPManuscript:foobar',
    bibliographicName: {
      _id: 'MPBibliographicName:foobar',
      objectType: 'MPBibliographicName',
    },
    sessionID: '40dc16f1-adbd-4410-af1c-2eddd6bfa63e',
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
  }

  t.ok(
    execute(Object.assign({}, validObject)).channel.calledWith(
      `${validObject.containerID}-metadata`
    )
  )
})
